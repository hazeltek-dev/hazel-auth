import pytest
from hazel_auth.interfaces import (
    IDbSession,
    ICredentialService,
    IUserRegistryService,
    IRegistrationService,
)
from hazel_auth.services import (
    AuthenticationError,
    RegistrationService,
    UserRegistryService,
)


class TestBase:
    def _register_user(self, context):
        user_dict = {
            'username': 'doe.john',
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'doe.john@mail.io',
            'password': 's3cr3t-pwd',
        }
        registry = context.registry
        service = registry.find_service(IRegistrationService, context)
        user = service.register(user_dict)
        assert user and user.uuid
        return user


class TestRegistrationService(TestBase):
    def test_password_hard_if_provided_during_registration(self, context):
        service = RegistrationService(context)
        user_dict = {
            'username': 'd.john',
            'first_name': 'John',
            'password': 'password',
        }
        user = service.register(user_dict)
        assert user.password is not None
        assert user.password != 'password'

    def test_user_inactive_if_activation_required(self, context):
        service = RegistrationService(context)
        user = service.register({'username': 'd.john', 'first_name': 'John'})
        assert user and user.uuid
        assert user.is_active is False
        assert user.is_activated() is False

    def test_user_active_if_activation_not_required(self, context):
        context['hazel.auth.require_activation'] = False
        service = RegistrationService(context)
        user = service.register({'username': 'd.john', 'first_name': 'John'})
        assert user and user.uuid
        assert user.is_active is True
        assert user.is_activated() is True

    def test_has_activity_code_if_activation_required(self, context):
        context['hazel.auth.require_activation'] = True
        service = RegistrationService(context)
        user_dict = {
            'username': 'd.john',
            'first_name': 'John',
            'password': 'password',
        }
        user = service.register(user_dict)
        assert user and user.uuid
        assert user.is_activated() is False
        assert user.activity_token is not None

    def test_no_activity_code_if_activation_not_required(self, context):
        context['hazel.auth.require_activation'] = False
        service = RegistrationService(context)
        user_dict = {
            'username': 'd.john',
            'first_name': 'John',
            'password': 'password',
        }
        user = service.register(user_dict)
        assert user and user.uuid
        assert user.is_activated() is True
        assert user.activity_token is None

    def test_can_activate_user_via_token(self, context):
        context['hazel.auth.require_activation'] = True
        service = RegistrationService(context)
        user_dict = {
            'username': 'd.john',
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'd.john@mail.io',
            'password': 's3krit',
        }
        user = service.register(user_dict)
        assert user and user.uuid
        assert user.is_activated() is False
        assert user.activity_token is not None

        token_code = user.activity_token.token
        activated = service.activate_by_token(token_code)
        assert activated and activated.uuid
        assert activated.is_activated() is True
        assert activated.activity_token is None

        model = service._meta.models.ActivityToken
        dbsession = context.registry.find_service(IDbSession)
        token = (
            dbsession.query(model).filter(model.token == token_code).first()
        )
        assert token is None

    def test_can_create_activity_token(self, context):
        registry = context.registry
        dbsession = registry.find_service(IDbSession)
        rservice = registry.find_service(IRegistrationService, context)

        model = rservice._meta.models.ActivityToken
        assert dbsession.query(model).count() == 0

        token, valid_for = rservice.create_activity_token()
        assert token is not None and valid_for > 0
        assert dbsession.query(model).count() == 0

    def test_creating_activity_token_for_user(self, context):
        self._register_user(context)
        registry = context.registry

        user_email = 'doe.john@mail.io'
        rservice = registry.find_service(IRegistrationService, context)
        (user, token, valid_for) = rservice.create_activity_token_for(
            user_email
        )
        assert user.activity_token is token
        assert token and token.uuid
        assert user and user.uuid
        assert valid_for > 0

        rrservice = registry.find_service(IUserRegistryService, context)
        ruser = rrservice.get_by_email(user_email)
        assert ruser and ruser.uuid
        assert ruser.activity_token is not None
        assert ruser.activity_token.token == token.token


class TestUserRegistryService(TestBase):
    def test_get_by_username(self, context):
        regservice = RegistrationService(context)
        user = regservice.register(
            {'username': 'd.john', 'first_name': 'John'}
        )
        assert user and user.uuid

        usrservice = UserRegistryService(context)
        rval = usrservice.get_by_username('d.john')
        assert rval and rval.uuid

    def test_get_by_email(self, context):
        regservice = RegistrationService(context)
        user_dict = {
            'username': 'd.john',
            'first_name': 'John',
            'email': 'd.john@mail.io',
        }
        user = regservice.register(user_dict)
        assert user and user.uuid

        usrservice = UserRegistryService(context)
        rval = usrservice.get_by_email('d.john@mail.io')
        assert rval and rval.uuid

    def test_get_by_authn_username(self, context):
        regservice = RegistrationService(context)
        user_dict = {
            'username': 'd.john',
            'first_name': 'John',
            'password': 's3krit',
            'email': 'd.john@mail.io',
        }
        user = regservice.register(user_dict)
        assert user and user.uuid

        usrservice = UserRegistryService(context)
        rval = usrservice.get_by_authn_username('d.john', 's3krit')
        assert rval and rval.uuid

        assert usrservice.get_by_authn_username('d.john', 'secret') is None
        assert usrservice.get_by_authn_username('dj', 's3krit') is None

    def test_get_by_authn_email(self, context):
        regservice = RegistrationService(context)
        user_dict = {
            'username': 'd.john',
            'first_name': 'John',
            'password': 's3krit',
            'email': 'd.john@mail.io',
        }
        user = regservice.register(user_dict)
        assert user and user.uuid

        usrservice = UserRegistryService(context)
        rval = usrservice.get_by_authn_email('d.john@mail.io', 's3krit')
        assert rval and rval.uuid

        assert (
            usrservice.get_by_authn_email('d.john@mail.io', 'secret') is None
        )
        assert usrservice.get_by_authn_email('d.john', 's3krit') is None


class TestCredentialService(TestBase):
    def test_password_reset(self, context):
        self._register_user(context)
        registry = context.registry

        rservice = registry.find_service(IUserRegistryService, context)
        authn_func = rservice.get_by_authn_username
        user = authn_func('doe.john', 's3cr3t-pwd')
        assert user and user.uuid and user.username == 'doe.john'

        cservice = registry.find_service(ICredentialService, context)
        cservice.reset_password(user, 'l0ck-pwd')

        assert authn_func('doe.john', 's3cr3t-pwd') is None
        assert authn_func('doe.john', 'l0ck-pwd') is not None

    def test_authn_for_unactivated_user_with_valid_cred_fails(self, context):
        registry = context.registry
        cservice = registry.find_service(ICredentialService, context)

        user = self._register_user(context)
        assert user and not user.is_activated()
        with pytest.raises(AuthenticationError):
            cservice.authenticate('doe.john', 's3cr3t-pwd')

    def test_authn_for_activated_user_with_bad_cred_fails(self, context):
        registry = context.registry
        rservice = registry.find_service(IRegistrationService, context)

        ## prepare
        # register and activate user
        user = self._register_user(context)
        assert user and not user.is_activated()

        auser = rservice.activate_by_token(user.activity_token.token)
        assert auser and auser.is_activated()

        ## test
        cservice = registry.find_service(ICredentialService, context)
        with pytest.raises(AuthenticationError):
            cservice.authenticate('doe.john', 'invalid-pwd')

    def test_authn_for_activate_user_with_valid_cred_passes(self, context):
        registry = context.registry
        rservice = registry.find_service(IRegistrationService, context)

        ## prepare
        # register and activate user
        user = self._register_user(context)
        assert user and not user.is_activated()

        auser = rservice.activate_by_token(user.activity_token.token)
        assert auser and auser.is_activated()

        ## test
        cservice = registry.find_service(ICredentialService, context)
        fuser = cservice.authenticate('doe.john', 's3cr3t-pwd')
        assert fuser and fuser.can_login()

    def test_can_auth_with_cred_if_activation_not_required(self, context):
        context['hazel.auth.require_activation'] = False
        user = self._register_user(context)
        assert user and user.can_login()

        registry = context.registry
        cservice = registry.find_service(ICredentialService, context)
        fuser = cservice.authenticate('doe.john', 's3cr3t-pwd')
        assert fuser and fuser.can_login()

    def test_cant_auth_with_bad_cred_if_activation_not_required(self, context):
        context['hazel.auth.require_activation'] = False
        user = self._register_user(context)
        assert user and user.can_login() is True

        registry = context.registry
        cservice = registry.find_service(ICredentialService, context)
        with pytest.raises(AuthenticationError):
            cservice.authenticate('doe.john', 'invalid-pwd')

    def test_can_auth_with_username_only_if_email_not_allowed(self, context):
        context['hazel.auth.require_activation'] = False
        context['hazel.auth.allow_email_auth'] = False
        user = self._register_user(context)
        assert user and user.can_login() is True

        registry = context.registry
        cservice = registry.find_service(ICredentialService, context)
        user1 = cservice.authenticate('doe.john', 's3cr3t-pwd')
        assert user1 and user1.can_login() is True

        with pytest.raises(AuthenticationError):
            cservice.authenticate('doe.john@mail.io', 's3cr3t-pwd')

    def test_can_auth_with_username_and_email_if_allowed(self, context):
        context['hazel.auth.require_activation'] = False
        context['hazel.auth.allow_email_auth'] = True
        user = self._register_user(context)
        assert user and user.can_login() is True

        registry = context.registry
        cservice = registry.find_service(ICredentialService, context)
        user1 = cservice.authenticate('doe.john', 's3cr3t-pwd')
        assert user1 and user1.can_login() is True

        user2 = cservice.authenticate('doe.john@mail.io', 's3cr3t-pwd')
        assert user2 and user2.can_login() is True
