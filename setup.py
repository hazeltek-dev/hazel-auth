# pylint: disable=invalid-name
import os.path as fs
from setuptools import setup, find_packages


HERE = fs.abspath(fs.dirname(__file__))


def readfile(name):
    with open(fs.join(HERE, name)) as f:
        return f.read()


requires = ['argon2_cffi', 'zope.interface', 'hazel==0.2', 'hazel-db==0.1.1']
devenv_require = ['pre-commit']
tests_require = ['pytest', 'pytest-cov']


setup(
    name='hazel-auth',
    version='0.2',
    description='Simple user registration and authentication library',
    long_description='\n\n'.join(
        [readfile('CHANGES.md'), readfile('README.md')]
    ),
    author='Hazeltek Solutions',
    author_email='Abdul-Hakeem Shaibu <hkmshb@gmail.com>',
    url='https://bitbucket.org/hazeltek-dev/hazel-auth.git',
    packages=find_packages(where='src', exclude=['tests']),
    package_dir={'': 'src'},
    include_package_data=True,
    python_requires='>=3.6',
    install_requires=requires,
    extras_require={'testing': tests_require, 'devenv': devenv_require},
    test_suite='tests',
    zip_safe=False,
    keywords='hazel toolkit',
    dependency_links=[
        'https://bitbucket.org/hazeltek-dev/hazel/get/v0.2.tar.gz#egg=hazel-0.2',
        'https://bitbucket.org/hazeltek-dev/hazel-db/get/v0.1.1.tar.gz#egg=hazel-db-0.1.1',
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
    ],
)
