import logging
from datetime import datetime, timedelta
from sqlalchemy import func
from zope.interface import implementer
from hazel.config import asbool
from .utils import generate_random_string
from .interfaces import (
    IDbSession,
    IPasswordHasher,
    IAuthModelMapping,
    ICredentialService,
    IUserRegistryService,
    IRegistrationService,
)

_log = logging.getLogger(__name__)  # pylint: disable=invalid-name


class AuthenticationError(Exception):
    """The exception raised when authentication fails.
    """


class ServiceBase:
    def __init__(self, context):
        self.context = context

    @property
    def _meta(self):
        propkey = '__meta'
        if not hasattr(self, propkey):
            model_dict = self.context.registry.find_service(IAuthModelMapping)
            expected = {'User', 'Role', 'ActivityToken'}
            found = expected & set(model_dict.keys())

            if not (found and len(found) == 3):
                msgfmt = "Invalid IAuthModelMapping. Expected Keys: {}"
                raise ValueError(msgfmt.format(', '.join(expected)))

            model_inst = type(
                'AuthModels', (), {name: model_dict[name] for name in found}
            )
            meta_inst = type('Meta', (), {'models': model_inst})
            setattr(self, propkey, meta_inst)
        return getattr(self, propkey)


@implementer(ICredentialService)
class CredentialService(ServiceBase):
    def authenticate(self, identifier, password):
        """Validates provided credentials then performs user pre-login checks.

        The credentials validation entails verifying the authenticity of the
        provided username or email and password. The user pre-login checks on
        the other hand ensure user account is in the appropriate state/status
        to permit access to resources.

        :param identifier: the username or email of user to authenticate.
        :type identifier: string

        :param password: the password for the user to authenticate.
        :type password: string
        """
        registry = self.context.registry
        config_key = 'hazel.auth.allow_email_auth'
        allow_email_auth = self.context.get(config_key, True)

        # verify credentials trying the username first
        service = registry.find_service(IUserRegistryService, self.context)
        user = service.get_by_authn_username(identifier, password)

        # verify credentials using email
        if allow_email_auth and not user:
            user = service.get_by_authn_email(identifier, password)

        if not user:
            raise AuthenticationError('Invalid credentials provided.')

        self._perform_prelogin_checks(user)
        return user

    def reset_password(self, user, password):
        """Reser user password and clear all pending activity tokens.
        """
        dbsession = self.context.registry.find_service(IDbSession)
        self.set_password(user, password)
        if not user.activated_at:
            user.activated_at = datetime.now()
        dbsession.delete(user.activity_token)
        dbsession.flush()

    def set_password(self, user, password):
        """Sets the hashed form of a password for the user.
        """
        hasher = self.context.registry.find_service(IPasswordHasher)
        hashed_pwd = hasher.hash_password(password)
        user.password = hashed_pwd

    def verify_password(self, user, password):
        """Checks the authenticity of the provider username or email and
        password.
        """
        if not user.password:
            return False

        hasher = self.context.registry.find_service(IPasswordHasher)
        return hasher.verify_password(password, user.password)

    def _perform_prelogin_checks(self, user):
        """Processes a user further to ensure account is in an appropriate
        state/status to permit access to resources.
        """
        config_key = 'hazel.auth.require_activation'
        require_activation = asbool(self.context.get(config_key, True))
        if require_activation and not user.is_activated():
            raise AuthenticationError(
                'Your account is not active, please check your email. If your '
                'account activation link has expired please request a password '
                'reset.'
            )

        if not user.can_login():
            msg = 'This user account cannot login at the moment'
            raise AuthenticationError(msg)


@implementer(IUserRegistryService)
class UserRegistryService(ServiceBase):
    def get_by_authn_email(self, email, password):
        user = self.get_by_email(email)
        if user and self.verify_password(user, password):
            return user
        return None

    def get_by_authn_username(self, username, password):
        user = self.get_by_username(username)
        if user and self.verify_password(user, password):
            return user
        return None

    def get_by_email(self, email):
        model = self._meta.models.User
        registry = self.context.registry
        dbsession = registry.find_service(IDbSession)
        user = (
            dbsession.query(model)
            .filter(func.lower(model.email) == email.lower())
            .first()
        )
        return user

    def get_by_username(self, username):
        model = self._meta.models.User
        registry = self.context.registry
        dbsession = registry.find_service(IDbSession)
        user = (
            dbsession.query(model)
            .filter(func.lower(model.username) == username.lower())
            .first()
        )
        return user

    def get_by_token(self, activity_token):
        model = self._meta.models.User
        registry = self.context.registry
        dbsession = registry.find_service(IDbSession)
        user = (
            dbsession.query(model)
            .filter(model.activity_token_id == activity_token.uuid)
            .first()
        )
        return user

    def verify_password(self, user, password):
        """Valiates provided password for user.
        """
        registry = self.context.registry
        service = registry.find_service(ICredentialService, self.context)
        return service.verify_password(user, password)


@implementer(IRegistrationService)
class RegistrationService(ServiceBase):
    def activate_by_token(self, token_code):
        """Activate a user using activation token sent to user email.
        """
        model = self._meta.models.ActivityToken
        dbsession = self.context.registry.find_service(IDbSession)
        activity_token = (
            dbsession.query(model).filter(model.token == token_code).first()
        )
        if not (activity_token and not activity_token.is_expired()):
            return None

        registry = self.context.registry
        service = registry.find_service(IUserRegistryService, self.context)
        user = service.get_by_token(activity_token)
        user.activated_at = datetime.now()
        user.is_active = True

        dbsession.delete(activity_token)
        del user.activity_token
        dbsession.flush()
        return user

    def create_activity_token(self):
        """Creates and returns an ActivityToken that can be associated with
        a user. Created token can be used for account activation, password
        reset or any other sensitive activity that requires activity to be
        verified against a target user.

        :return: tuple of (activity_token, expiration_ducation in seconds)
        """
        config_key = 'hazel.auth.token_valid_for'
        valid_for = int(self.context.get(config_key, 24 * 3600))
        activity_token = self._meta.models.ActivityToken(
            expires_at=(datetime.now() + timedelta(seconds=valid_for)),
            token=generate_random_string(32),
        )
        return (activity_token, valid_for)

    def create_activity_token_for(self, email):
        """Creates an ActivityToken for the user identified by the specified
        email if found otherwise None is returned.

        :return: (user, token, expiration-timedelta) otherwise None
        if user is disabled or can't be found by email.
        """
        registry = self.context.registry
        service = registry.find_service(IUserRegistryService, self.context)
        user = service.get_by_email(email)
        assert user, (
            "Tried creating activity token for non-existing user with "
            "email: `%s`" % email
        )

        if not user:
            return (None, None, 0)

        token, valid_for = self.create_activity_token()
        user.activity_token = token

        # persist activity_token
        dbsession = registry.find_service(IDbSession)
        dbsession.add(user)
        dbsession.flush()

        return (user, token, valid_for)

    def register(self, data_dict):
        """Registers a new user.
        """
        # pylint: disable=fixme
        # TODO: data validation
        model = self._meta.models.User
        password = data_dict.pop('password', None)
        user = model(**data_dict)

        registry = self.context.registry
        service = registry.find_service(ICredentialService, self.context)
        if password:
            service.set_password(user, password)

        # auto activate if config settings says to
        config_key = 'hazel.auth.require_activation'
        require_activation = asbool(self.context.get(config_key, True))
        user.is_active = not require_activation
        if not require_activation:
            user.activated_at = datetime.now()
        else:
            activity_token, _ = self.create_activity_token()
            user.activity_token = activity_token

        dbsession = registry.find_service(IDbSession)
        dbsession.add(user)
        dbsession.flush()
        return user.uuid and user

    def send_activation_email(self, user):
        """Create and send your account activation mail.
        """
        pass
