import pkg_resources


def get_version():
    '''Retrieves and returns the package version details.
    '''
    package = pkg_resources.require('hazel-auth')
    return package[0].version


def configure_models():
    from hazel_db import meta
    from . import models

    meta.attach_model(models.User, meta.BASE)
    meta.attach_model(models.Role, meta.BASE)
    meta.attach_model(models.UserRole, meta.BASE)
    meta.attach_model(models.ActivityToken, meta.BASE)


def configure_services(context):
    # pylint: disable=too-many-locals
    from hazel.services import IContext
    from .interfaces import (
        IPasswordHasher,
        IAuthModelMapping,
        ICredentialService,
        IRegistrationService,
        IUserRegistryService,
    )
    from .services import (
        CredentialService,
        RegistrationService,
        UserRegistryService,
    )
    from .models import User, Role, ActivityToken
    from .crypt import Argon2Hasher

    regservice = context.registry.register_service
    regfactory = context.registry.register_factory

    regservice(Argon2Hasher(), IPasswordHasher)
    regfactory(CredentialService, ICredentialService, IContext)
    regfactory(RegistrationService, IRegistrationService, IContext)
    regfactory(UserRegistryService, IUserRegistryService, IContext)
    regservice(
        {cls.__name__: cls for cls in (User, Role, ActivityToken)},
        IAuthModelMapping,
    )
