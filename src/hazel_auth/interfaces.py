# pylint: disable=inherit-non-class, no-self-argument, no-method-argument
from zope.interface import Attribute, Interface


class IAuthModelMapping(Interface):
    """A marker interface for a mapping of auth models.
    """


class IDbSession(Interface):
    """A market interface for a database session handle.
    """


class IPasswordHasher(Interface):
    """A utility for hashing passwords.
    """

    def hash_password(plain_text):
        """Generate a hash presentation for the provided plain text password.
        """
        pass

    def verify_password(plain_text, hashed_password):
        """Verify a password. Hashes provided plain_text and compares it to
        the persisted hash.
        """
        pass


class IUser(Interface):
    """Defines the default fields required of a user model.
    """

    friendly_name = Attribute('friendly_name')
    fullname = Attribute('fullname')
    username = Attribute('username')
    email = Attribute('email')


class IRole(Interface):
    """Defines the default fields required of a role model.
    """

    name = Attribute('name')
    description = Attribute('description')


class IActivityToken(Interface):
    """Defines the default fields required of an activity token.
    """

    token = Attribute('token')
    expires_at = Attribute('expires_at')


class ICredentialService(Interface):
    """A service for managing user passwords and tokens related activities.
    """

    def authenticate(identifier, password):
        """Validates provided credentials then performs user pre-login checks.

        The credentials validation entails verifying the authenticity of the
        provided username or email and password. The user pre-login checks on
        the other hand ensure user account is in the appropriate state/status
        to permit access to resources.

        :param identifier: the username or email of user to authenticate.
        :type identifier: string

        :param password: the password for the user to authenticate.
        :type password: string
        """
        pass

    def reset_password(user, password):
        """Reser user password and clear all pending activity tokens.
        """
        pass

    def set_password(user, password):
        """Sets the hashed form of a password for the user.
        """
        pass

    def verify_password(user, password):
        """Validates provided password for the user.
        """
        pass


class ILoginService(Interface):
    """A service responsible for handling authentication actions.
    """

    def login(identifier, password):
        """Authenticates the provided credentials and if valid and creates a session
        for the identified user.
        """
        pass

    def logout():
        """Terminates the current session.
        """
        pass


class IUserRegistryService(Interface):
    """A service responsible for managing user queries.
    """

    def get_by_authn_email(email, password):
        pass

    def get_by_authn_username(username, password):
        pass

    def get_by_email(email):
        pass

    def get_by_username(username):
        pass


class IRegistrationService(Interface):
    """A service responsible for managing user registration and creation.
    """

    def activate_by_token(activity_token, location):
        """Activate a user using activation token sent to user email.
        """
        pass

    def create_activity_token():
        """Creates and returns an ActivityToken that can be associated with
        a user. Created token can be used for account activation, password
        reset or any other sensitive activity that requires activity to be
        verified against a target user.

        :return: tuple of (activity_token, expiration_ducation in seconds)
        """
        pass

    def create_activity_token_for(email):
        """Creates an ActivityToken for the user identified by the specified
        email if found otherwise None is returned.

        :return: (user, token, expiration-timedelta) otherwise None
        if user is disabled or can't be found by email.
        """
        pass

    def register(data_dict):
        """Registers a new user.
        """
        pass

    def send_activation_email(user):
        """Create and send user account activation mail.
        """
        pass
