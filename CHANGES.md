# CHANGES

## Version: 0.2

- Re-implement credentials, registration and user registry services
- Make package Pip installable

## Version: 0.1

- Initial release
